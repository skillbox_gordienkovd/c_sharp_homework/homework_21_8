﻿using homework_20_7.Models;
using homework_20_7.Repository;

namespace homework_20_7.Services;

public class AccountService
{
    public static List<Account> FindAll()
    {
        using NotebookDbContext dbContext = new();
        
        return dbContext.Accounts.ToList();
    }

    public static Account FindById(long id)
    {
        using NotebookDbContext dbContext = new();
        
        return dbContext.Accounts.First(account => account.ID == id);
    }

    public static void Create(Account account)
    {
        using NotebookDbContext dbContext = new();

        dbContext.Accounts.Add(account);
        dbContext.SaveChanges();
    }

    public static void Update(Account account)
    {
        using NotebookDbContext dbContext = new();

        dbContext.Accounts.Update(account);
        dbContext.SaveChanges();
    }

    public static void Delete(long id)
    {
        using NotebookDbContext dbContext = new();
        
        var account = dbContext.Accounts.First(account => account.ID == id);

        dbContext.Accounts.Remove(account);
        dbContext.SaveChanges();
    }
}