﻿using homework_20_7.Models;
using homework_20_7.Services;
using Microsoft.AspNetCore.Mvc;

namespace homework_20_7.Controllers;

public class AccountController : Controller
{
    private readonly AccountService _accountService = new AccountService();

    [HttpGet]
    public IActionResult Index()
    {
        return View(AccountService.FindAll());
    }

    [HttpGet]
    public IActionResult Details(long id)
    {
        return View(AccountService.FindById(id));
    }
    
    [HttpGet]
    public IActionResult Add()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Add(Account account)
    {
        AccountService.Create(account);

        return Redirect("~/Account/Index");
    }
    
    [HttpGet]
    public IActionResult Edit(long id)
    {
        return View(AccountService.FindById(id));
    }
    
    [HttpPost]
    public IActionResult Edit(Account account)
    {
        AccountService.Update(account);
        
        return Redirect("~/Account/Index");
    }
    
    [HttpGet]
    public IActionResult Delete(long id)
    {
        AccountService.Delete(id);
        
        return Redirect("~/Account/Index");
    }
}