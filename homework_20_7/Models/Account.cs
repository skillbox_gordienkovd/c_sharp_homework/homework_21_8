﻿using System.ComponentModel.DataAnnotations;

namespace homework_20_7.Models;

public class Account
{
    public long ID { get; set; }

    [Required] public string Firstname { get; set; }

    [Required] public string Patronomyc { get; set; }

    [Required] public string Lastname { get; set; }

    [Required] public string Phone { get; set; }

    [Required] public string Address { get; set; }
}