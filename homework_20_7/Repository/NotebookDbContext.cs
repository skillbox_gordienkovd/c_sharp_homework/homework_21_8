﻿using homework_20_7.Models;
using Microsoft.EntityFrameworkCore;

namespace homework_20_7.Repository;

public sealed class NotebookDbContext: DbContext
{
    public DbSet<Account> Accounts { get; set; }
    
    public NotebookDbContext()
    {
        Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=notebook;Trusted_Connection=True;");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>().HasData(
            new Account
            {
                ID = 1,
                Firstname = "Иван",
                Patronomyc = "Иванович",
                Lastname = "Иванов",
                Phone = "+79123456781",
                Address = "г. Краснодар, ул. Ленина, д. 1, кв. 1"
            },
            new Account
            {
                ID = 2,
                Firstname = "Петр",
                Patronomyc = "Петрович",
                Lastname = "Петров",
                Phone = "+79123456782",
                Address = "г. Краснодар, ул. Ленина, д. 2, кв. 2"
            },
            new Account
            {
                ID = 3,
                Firstname = "Сидор",
                Patronomyc = "Сидорович",
                Lastname = "Сидоров",
                Phone = "+79123456783",
                Address = "г. Краснодар, ул. Ленина, д. 3, кв. 3"
            }
        );
    }
}